import { Component, OnInit, Input } from '@angular/core';
import {PostsService} from '../posts.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.scss']
})
export class PostListComponent implements OnInit {

  private posts:any[]

  constructor(private postService:PostsService) {
  }




  ngOnInit() {
    this.posts = this.postService.posts;
  }

}
