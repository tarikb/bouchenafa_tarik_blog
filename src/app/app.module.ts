import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { PostListComponent } from './post-list/post-list.component';
import {PostsService} from './posts.service';
import { NewPostComponentComponent } from './new-post-component/new-post-component.component';
import { NavbarComponent } from './navbar/navbar.component';
import {RouterModule, Routes} from '@angular/router';
import { NewComponent } from './new/new.component';
import {FormsModule} from '@angular/forms';
import { PostComponent } from './post/post.component';
import { PostListItemComponent } from './post-list-item/post-list-item.component';

const appRoutes: Routes = [
  { path: 'posts', component: PostListComponent},
  { path: '', component: PostListComponent},
  { path: 'new', component: NewComponent}
]
@NgModule({
  declarations: [
    AppComponent,
    PostListComponent,
    NewPostComponentComponent,
    NavbarComponent,
    NewComponent,
    PostComponent,
    PostListItemComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [
    PostsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
