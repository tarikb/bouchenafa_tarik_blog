import { Injectable } from '@angular/core';
import {AppComponent} from './app.component';

@Injectable()
export class PostsService {

  public posts:any[] = [
    { title:  ' Mon premier post', content: 'Ceci est mon premier post', loveIts: 0, created_at: 'date', },
    { title:  ' Mon deuxième post', content: 'Ceci est mon deuxième post', loveIts: 0, created_at: 'date', },
    { title:  ' Un autre post', content: 'Ceci est un autre post', loveIts: 0, created_at: 'date', },
  ];

  constructor() { }

}
