import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  posts = [
    {
      title:  ' Mon premier post',
      content: 'Ceci est mon premier post',
      loveIts: 0,
      created_at: 'date',
    },
    {
      title:  ' Mon deuxième post',
      content: 'Ceci est mon deuxième post',
      loveIts: 0,
      created_at: 'date',
    },
    {
      title:  ' Un autre post',
      content: 'Ceci est un autre post',
      loveIts: 0,
      created_at: 'date',
    }
  ];
}
